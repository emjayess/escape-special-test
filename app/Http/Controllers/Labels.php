<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Labels extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $labels = [
            'latin1' => DB::table('labels_in_latin1')->get()->first()
            ,
            'latin2' => DB::table('labels_in_latin2')->get()->first()
            ,
            'utf8' => DB::table('labels_in_utf8')->get()->first()
            ,
            'utf8mb4' => DB::table('labels_in_utf8mb4')->get()->first()
            ,
        ];
        
        return $labels;
        # json_encode($labels, JSON_UNESCAPED_UNICODE);
        #return response()->json($labels, 200, [], JSON_UNESCAPED_UNICODE);
    }
}
