<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }
/*
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }*/

            .content {
                display: flex;
                flex-direction: row;
                padding: 20px;
                text-align: center;
            }

            .content > div {
                flex: 1 1 auto;
                margin: 20px;
            }

            .title {
                font-size: 42px;
            }

            h1 {
                background-color: #cee;
            }
            code {
                background-color: #cec;
            }
        </style>
    </head>
    <body>
        <div id="app" class="full-height">
            <div class="content">
                <div class="blade-labels">
                    <h1>Blade Label(s):</h1>

                    @foreach ( $labels as $table_charset => $label_entry )
                        <div class="text-muted">
                            <code>{{ $table_charset }}</code> – rendered by blade (php):
                        </div>
                        <div class="title m-b-md">
                            {{ $label_entry->label }}
                        </div>
                    @endforeach
                </div>

                <label-component/>
            </div>
        </div>

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
